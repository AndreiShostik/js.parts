window.engine = (function() {
	var _this;

	function Engine() { // constructor
		_this = this;
		this.init();
	}

	Engine.module = function () { // static
		return this.$$bridge
	};

	Engine.prototype = { // prototype ?
		version: "2.0",
		$$bridge: $({}),
		$document: $(document),
		$window: $(window),
		init: function () {
			this.setOptions();
			this.bindEvents();
		},
		setOptions: function () {},
		bindEvents: function () {
			this.$$bridge.on('Engine.tmpl', function(e, sTemplate, oData) { return _this.tmpl(sTemplate, oData) });
			this.$$bridge.on('Engine.initModule', function(e, data) { return _this.initModule(data.oModule, data.sParent, data.parameters); });
		},
		tmpl: function () {},
		initModule: function (oModule, sParent, parameters) {
			var name = oModule.name;

			if (sParent && sParent != this.constructor.name && this[sParent]) {
				if (this[sParent][name]) {
					this[sParent][name].setOptions();
					return this[sParent][name];
				} else {
					this.extend(oModule, this[sParent].constructor);
					this[sParent][name] = new oModule(parameters);
					//this.resolveDependencies(sParent);
				}
				//} else {
				//	this.cacheModule(oModule, sParent, parameters);
				//}
			} else {
				if (this[name]) {
					this[name].setOptions();
					return this[name];
				}
				else {
					this.extend(oModule, this.constructor);
					this[name] = new oModule(parameters);
					//this.resolveDependencies(name);
				}
			}
		},
		extend: function (oChild, oParent) {
			var F = function() {};
			F.prototype = oParent.prototype;
			oChild.prototype = new F();
			oChild.prototype.constructor = oChild;
			oChild.parent = oParent.prototype;
		}
	};

	return new Engine();
}());